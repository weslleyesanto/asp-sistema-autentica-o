<!--#include file="include\config.asp"-->
<%

Dim retorno
Response.ContentType = "application/json"

If Request.ServerVariables("REQUEST_METHOD") = "POST" Then

	Dim email, senha
	Dim nivel, url

	email = LimparTexto(Request.Form("email"))
	senha = LimparTexto(Request.Form("senha"))

	if IsEmpty(email) OR IsEmpty(senha) Then
		if IsEmpty(email) Then
			Response.Write("Preencha o campo e-mail!")
			response.end
		else
			if validaEmail(email) <> True Then
				Response.Write("Preenchimento do campo e-mail incorreto!")
				response.end
			end if
			'FINAL VERIFICA E-MAIL VÁLIDO'
		end if
		'FINAL VERIFICA E-MAIL EM BRANCO'
		if IsEmpty(senha) Then
			Response.Write("Preencha o campo senha!")
			response.end
		end if
		'FINAL VERIFICA SENHA EM BRANCO'
	else
		'QUERY VERIFICAR SE USUARIO EXISTE'
		TABELA = "tb_usuario "
		WHERE = " WHERE nm_email = '"& email & "' AND ds_senha = MD5('" & senha &"') AND ic_status = 1"

		set retorno_query = get_query(TABELA, false, WHERE, false, false)

		If not retorno_query.EOF Then 'VERIFICA SE TRUE

			Do While Not retorno_query.EOF

				id_usuario = retorno_query.Fields("id_usuario").Value
				nm_usuario = retorno_query.Fields("nm_usuario").Value
				nm_email = retorno_query.Fields("nm_email").Value
				ds_senha = retorno_query.Fields("ds_senha").Value
				nivel = retorno_query.Fields("ic_usuario").Value
				ic_logado = 1
				ip = Request.ServerVariables("REMOTE_ADDR")
				retorno_query.MoveNext
			Loop
			retorno_query.close

			'INICIO UPDATE TB_USUARIO PARA LOGADO
			TABELA = "tb_usuario"
			PARAM = " SET ic_logado = 1 "
			WHERE = " WHERE id_usuario = " & id_usuario
			
			call update(TABELA, PARAM, WHERE)

			'Salvar no banco de dados o log
			TABELA = "tb_log"
			PARAM = "(id_usuario, ip) VALUES (" & id_usuario &", '" & ip & "')"
			insert_row = insert(TABELA, PARAM)

			If insert_row <> "" Then 'INSERIU NO BANCO SE TRUE

				'criar as variaveis de sessão
				session("autenticado") = "S"
				session("id_usuario") = id_usuario
				session("nm_usuario") = nm_usuario
				session("nm_email") = nm_email
				session("ic_logado") = ic_logado
				session("nivel") = nivel

				IF nivel = 1 Then 'verificar se nivel é admin
					url = "painel_admin.asp"
				elseif nivel = 2 Then 'verifica se nivel é usuário
					url = "painel_usuario.asp"
				else
					url = "default.asp"
				End If
				'FINAL VERIFICAÇÃO DE NIVEIS
				retorno = "{""res"":""ok"", ""msg"":""Usuário encontrado!"", ""url"": """&url&"""}"
			else
				url = "login.asp"
				retorno = "{""res"":""error"", ""msg"":""Erro ao salvar LOG!"", ""url"": """&url&"""}"
			End if
			'FINAL INSERT TB_LOG'
			Response.Write(retorno)
		else
			url = "login.asp"
			retorno = "{""res"":""error"", ""msg"":""Usuário não encontrado!"", ""url"": """&url&"""}"
			Response.Write(retorno)
		end if
		'final rsTemp'
	end if
	'FINAL IF VERIFICA SE E-MAIL OU SENHA ESTÁ EM BRANCO'
else 
	Response.Redirect "login.asp"
End if 
'FINAL VERIFICA POST'

%>