<!--#include file="include\config.asp"-->

<% call verificarSession %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>

</head>

<body>

    <div id="wrapper">

     <!--#include file="include\header.asp"-->

     <div id="page-wrapper">

        <div class="container-fluid">
            <h1 class="page-header">Efetue o login</h1>
            <div class="alert alert-success" id="alert" style="display:none;"> </div>

            <form role="form" name="form_login" method="post" id="form_login">

                <label>E-mail</label>
                <input class="form-control" maxlength="100" required="required" placeholder="seu@email.com" id="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"/>

                <label>Senha</label>
                <input type="password" class="form-control" required="required" placeholder="Senha" id="senha" name="senha" />


                <button type="submit" class="btn btn-default" name="acessar" id="btnEntrar">Acessar</button>

            </form>

        </div>
    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<!--#include file="include\js.asp"-->
<script src="assets/js/login.js"></script>
</body>

</html>
