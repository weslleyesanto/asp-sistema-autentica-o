<%
'Arquivo criado para criar funções que seram usadas dentro do sistema
Function verificarSessionUsuario(n)

	LOGIN = "login.asp"
	LOGOUT = "logout.asp"

	if Session("autenticado") = "S" then

		TABELA = "tb_usuario"
		WHERE = " WHERE id_usuario = " & Session("id_usuario") & " AND " & " ic_usuario = " & n
		set retorno_query = get_query(TABELA, false, WHERE, false, false)

		If not retorno_query.EOF Then 'VERIFICA SE TRUE
			Do While Not retorno_query.EOF
				nivel = retorno_query.Fields("ic_usuario").Value
				ic_logado = retorno_query.Fields("ic_logado").Value
				retorno_query.MoveNext
			Loop
			retorno_query.close

			if ic_logado <> 1 then 'VERIFICA NO BANCO SE USURIO ESTÁ LOGADO
				Response.Redirect LOGOUT
			end if
			'FINAL IF IC_LOGADO'
		else
			verificarSession
		End if
		'FINAL IF RES.EOF
	else
		Response.Redirect LOGOUT
	End if
	'FINAL SE EXISTE SESSION AUTENTICADO IGUAL A "S"
End Function

Function verificarSession

	if 	session("autenticado") = "S" then

		nivel = session("nivel")

		IF nivel = 1 Then 'verificar se nivel é admin
			Response.Redirect "painel_admin.asp"
		elseif nivel = 2 Then 'verifica se nivel é usuário
			Response.Redirect "painel_usuario.asp"
			'response.End	
		End If
	End if

End Function

Function acessosUsuario(id_usuario)

	TABELA = "tb_log"
	WHERE = " WHERE id_usuario = "& id_usuario
	PARAM = " , COUNT(*) as num_acessos"

	set retorno_query = get_query(TABELA, PARAM, WHERE, false, false)

	If not retorno_query.EOF Then 'VERIFICA SE TRUE

		Do While Not retorno_query.EOF
			num_acessos = retorno_query.Fields("num_acessos").Value
			retorno_query.MoveNext
		Loop
		retorno_query.close

	End if
		Response.write(num_acessos)

End Function

Function validaEmail(ByVal strEmail)
	Dim regEx
	Dim ResultadoHum
	Dim ResultadoDois 
	Dim ResultadoTres
	Set regEx = New RegExp          
	regEx.IgnoreCase = True        
	regEx.Global = True             

	regEx.Pattern	= "[^@\-\.\w]|^[_@\.\-]|[@\.]{2}|(@)[^@]*\1"
	ResultadoHum	= RegEx.Test(strEmail)

	regEx.Pattern	= "@[\w\-]+\."        
	ResultadoDois	= RegEx.Test(strEmail)

	regEx.Pattern	= "\.[a-zA-Z]{2,3}$"  
	ResultadoTres	= RegEx.Test(strEmail)
	Set regEx = Nothing

	If Not (ResultadoHum) And ResultadoDois And ResultadoTres Then
		validaEmail = True
	Else
		validaEmail = False
	End If
End Function


function LimparTexto(str)
	str = trim(str)
	str = lcase(str)
	str = replace(str,"=","")
	str = replace(str,"'","")
	str = replace(str,"""""","")
	str = replace(str," or ","")
	str = replace(str," and ","")
	str = replace(str,"(","")
	str = replace(str,")","")
	str = replace(str,"<","[")
	str = replace(str,">","]")
	str = replace(str,"update","")
	str = replace(str,"-shutdown","")
	str = replace(str,"--","")
	str = replace(str,"'","")
	str = replace(str,"#","")
	str = replace(str,"$","")
	str = replace(str,"%","")
	str = replace(str,"¨","")
	str = replace(str,"&","")
	str = replace(str,"'or'1'='1'","")
	str = replace(str,"--","")
	str = replace(str,"insert","")
	str = replace(str,"drop","")
	str = replace(str,"delet","")
	str = replace(str,"xp_","")
	str = replace(str,"select","")
	str = replace(str,"*","")
	LimparTexto = str
end function

Function get_query(TABELA, PARAM, WHERE, INNER, GROUP_BY)
	On Error Resume Next
		''INSTACIA O OBJETO ADODB.Connection'
		Set Connect = Server.CreateObject("ADODB.Connection")
		Connect.CursorLocation = 3
		'ABRE CONEXAO COM O  BANCO DE DADOS'
		Connect.Open dB

		'Instancia o objeto do recordset
		Set TEMP = Server.CreateObject("ADODB.Recordset")

		'VERIFICA SE PARAM É FALSE'
		if PARAM = false Then
			PARAM = ""
		end if

		'VERIFICA SE INNER É FALSE'
		if INNER = false then
			INNER = ""
		End if

		'VERIFICA SE GROUP_BY É FALSE'
		if GROUP_BY = false then
			GROUP_BY = ""
		End if

		'MONTANDO QUERY'
		QUERY = "SELECT * " & PARAM & " FROM " & TABELA & INNER & WHERE & GROUP_BY

		'response.write(QUERY)
		'Response.End

		'EXECUTA QUERY'
		TEMP.Open QUERY, Connect, 3, 1
		
		'ENCERRANDO OBJETO ADODB.Recordset'
		Set TEMP.ActiveConnection = Nothing

		'ENCERRANDO OBJETO ADODB.Connection'
		Connect.Close
		Set Connect = Nothing	

		'RECEBENDO E RETORNANDO A CONSULTA'
		Set get_query = TEMP


	On Error Goto 0	
end function

function update(TABELA, PARAM, WHERE)
	On Error Resume Next
		''INSTACIA O OBJETO ADODB.Connection ANTES DE ENTRAR NESSA FUNCTION'
		Set Connect = Server.CreateObject("ADODB.Connection")
		Connect.CursorLocation = 3
		Connect.Open db

		'MONTANDO QUERY'
		QUERY = "UPDATE " & TABELA & PARAM & WHERE

		'EXECUTA QUERY'
		Connect.execute(QUERY)

		'ENCERRANDO OBJETO ADODB.Connection'
		Connect.Close
		Set Connect = Nothing
	On Error Goto 0	
end function

function insert(TABELA, PARAM)
	On Error Resume Next
		''INSTACIA O OBJETO ADODB.Connection'
		Set Connect = Server.CreateObject("ADODB.Connection")
		Connect.CursorLocation = 3
		Connect.Open db

		'MONTANDO QUERY'
		QUERY = "INSERT INTO "& TABELA & PARAM

		'EXECUTA QUERY'
		Connect.execute QUERY, RecordsAffected

		codigo_inserido = Connect.Execute( "SELECT @@IDENTITY" )(0).Value

	    'ENCERRANDO OBJETO ADODB.Connection'
		Connect.Close
		Set Connect = Nothing

		'RECEBENDO E RETORNANDO A CONSULTA'
	    insert = codigo_inserido


    On Error Goto 0	
end function

%>