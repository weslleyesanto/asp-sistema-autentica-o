<%@language="vbscript" codepage="65001"%>
<!--#include file="include\config.asp"-->
<% call verificarSessionUsuario(2) %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Painel Usuário</title>

    </head>

    <body>

        <div id="wrapper">

           <!--#include file="include\header.asp"-->

           <div id="page-wrapper">

            <div class="container-fluid">


                <h1>Painel Usuario</h1>
                
                <h2>Hello, <%= session("nm_usuario") %>!</h2>
                <p>Você acessou <% call acessosUsuario(session("id_usuario")) %> vezes o sistema!</p>
                
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <!--#include file="include\js.asp"-->
</body>

</html>
