var login = (function() {
	var L = {}, winH, winW;

	L.setDHTML = function() {
		$(function() {
			$('#form_login').submit( function(e){
				e.preventDefault();
			});
		});

		$('#btnEntrar').on('click', function() {
			L.login();
		});

	};

	L.login = function() {

		var email = $('#email').val();
		var senha = $('#senha').val();
		var error = false;

		if (email == '') {
			$('#email').addClass('alert-danger');
			error = error ? error : true;
		}

		if (senha == '') {
			$('#senha').addClass('alert-danger');
			error = error ? error : true;
		}

		if(!error){
			var data = {email:email, senha:senha};

			$.post('login_post.asp',data, function(ret){
				if(ret.res == 'ok'){
					window.location.href = ret.url;
				}
				else{
					$('#alert')
					.addClass('alert-danger')
					.css({'display':'block'})
					.html('<strong>Error!</strong> '+ret.msg);
					return false;
				}

			}, 'json');
		}
	}


	$(function() {
		L.setDHTML();
	});

	return L;
})();