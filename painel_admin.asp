<%@language="vbscript" codepage="65001"%>
<!--#include file="include\config.asp"-->
<% 
call verificarSessionUsuario(1)

    TABELA = "tb_log l"
    INNER = " INNER JOIN tb_usuario u ON l.id_usuario = u.id_usuario"
    GROUP_BY = " GROUP BY u.id_usuario"
    PARAM = " , COUNT(*) as num_acessos "
    set retorno_query = get_query(TABELA, PARAM, WHERE, INNER, GROUP_BY)
    %>

    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Painel Administrativo</title>

    </head>

    <body>

        <div id="wrapper">

         <!--#include file="include\header.asp"-->

         <div id="page-wrapper">

            <div class="container-fluid">

                <h1>Painel Administrativo</h1>

                <div id="listaUsuarios">
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Data/Hora</th>
                                <th>Usuario</th>
                                <th>IP</th>
                                <th>Número de acessos</th>
                                <th>Logado?</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%  If not retorno_query.EOF Then 'VERIFICA SE TRUE
                            Do While Not retorno_query.EOF

                            ic_logado = retorno_query.Fields("ic_logado").Value

                                if ic_logado = 1 then 'esta logado'
                                value = "Sim"    
                                elseif ic_logado = 0 then
                                value = "Não"
                                end if
                                %>
                                <tr data-id="<%= retorno_query.Fields("id_usuario").Value %>">
                                    <td><%= retorno_query.Fields("dt_log").Value %></td>
                                    <td><%= retorno_query.Fields("nm_usuario").Value %></td>
                                    <td><%= retorno_query.Fields("ip").Value %></td>
                                    <td><%= retorno_query.Fields("num_acessos").Value %></td>
                                    <td><button type="button"><%=value%></button></td>
                                    <td>
                                        <a href="#" title="Desconectar usuário" id="desconectarUsuario">Desconectar</a>
                                    </td>
                                </tr>
                                <%  
                                retorno_query.MoveNext
                                Loop
                                retorno_query.close

                                end if
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!--#include file="include\js.asp"-->
    </body>

    </html>
